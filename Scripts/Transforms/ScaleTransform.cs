﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTransform : Transformation
{
    public Vector3 scale;
    public override Vector3 Apply(Vector3 point)
    {
        scale.x *= point.x;
        scale.y *= point.y;
        scale.z *= point.z;
        return scale;
    }
}
