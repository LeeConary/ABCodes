﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

public class Tools
{
    public static string DataPath{
        get {
           string game = AppConst.AppName.ToLower();
           if (Application.isMobilePlatform)
           {   
               return Application.persistentDataPath;
           }
           if (AppConst.DebugMode)
           {
               string path = Application.dataPath + "/" + AppConst.AssetDir + "/";
               return path;
           }
           if (Application.platform == RuntimePlatform.OSXEditor)
           {
               int i = Application.dataPath.LastIndexOf('/');
               return Application.dataPath.Substring(0, i + 1);
           }
           return "/Users/etnly-/Desktop/" + game + "/"; 
        }
    }
    public static string GetRelativePath()
    {
        if (Application.isEditor)
            return "file://" + System.Environment.CurrentDirectory.Replace("\\", "/") + "/Assets/" + AppConst.AssetDir + "/";
        else if (Application.isMobilePlatform || Application.isConsolePlatform)
            return "file:///" + DataPath;
        else // For standalone player.
            return "file://" + Application.streamingAssetsPath + "/";
    }
}
