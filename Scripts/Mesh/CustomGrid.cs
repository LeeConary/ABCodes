﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class CustomGrid : MonoBehaviour
{
    public int xSize, ySize;
    public Vector3[] vertices;
    Vector2[] uv;

    Mesh mesh;
    private void Awake() 
    {
        StartCoroutine(GenerateGrids());
        //GenerateGrids();
    }

    IEnumerator GenerateGrids()
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Test mesh";
        vertices = new Vector3[(xSize + 1) * (ySize + 1)];
        uv = new Vector2[vertices.Length];

        // int[] triangles = new int[6];
        // triangles[0] = 0;
        // triangles[1] = xSize + 1;
        // triangles[2] = 1;
        // triangles[3] = xSize + 2;
        // triangles[4] = 1;
        // triangles[5] = xSize + 1;

        int[] triangles = new int[xSize * ySize * 6];
        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                vertices[i] = new Vector3(x, y);
                uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
                yield return new WaitForSeconds(0.1f);
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;
        
        //每一行最后一个顶点不会生成一个面
        //在到下一行时，需要将起始顶点的位置挪到下一行
        //因此三角形的索引需要加1
        for (int y = 0, ti = 0, vi = 0; y < ySize; y++ , vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 1] = triangles[ti + 5] = xSize + vi + 1;
                triangles[ti + 2] = triangles[ti + 4] = vi + 1;
                triangles[ti + 3] = xSize + vi + 2;
                mesh.triangles = triangles;
                yield return new WaitForSeconds(0.1f);
            }
        }
        //mesh.triangles = triangles;
        //偷个懒，用原生api生成法线
        mesh.RecalculateNormals();
    }

    void OnDrawGizmos()
    {
        if (vertices == null) return;
        Gizmos.color = Color.black;
        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], 0.05f);
        }   
    }
}
