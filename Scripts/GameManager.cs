﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UObject = UnityEngine.Object;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    Dictionary<string, AssetBundle> assets = new Dictionary<string, AssetBundle>();
    // Start is called before the first frame update
    void Start()
    {
        ResourceManagerNew.instance.Init(InitializeOKCallBack);
    }

    void InitializeOKCallBack() 
    {
        AssetBundle bundle = ResourceManagerNew.instance.GetBundleFromLoadedBundles("dragonBoatAct");
        Debug.Log(bundle.GetAllAssetNames().Length);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
