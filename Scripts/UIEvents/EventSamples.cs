﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSamples : MonoBehaviour
{
    public void OnClick1()
    {
        Debug.Log("Click 1");
    }

    public void OnClick2()
    {
        Debug.Log("Click 2");
    }

    public void OnLongPress()
    {
        Debug.Log("Long Press");
    }
}
