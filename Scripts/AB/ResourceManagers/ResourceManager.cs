﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UObject = UnityEngine.Object;

public class AssetBundleInfo
{
    public AssetBundle m_AssetBundle;
    public int m_ReferencedCount;

    public AssetBundleInfo(AssetBundle assetBundle)
    {
        m_AssetBundle = assetBundle;
        m_ReferencedCount = 0;
    }
}

class LoadAssetRequest
{
    public Type assetType;
    public string[] assetNames;
    // public LuaFunction luaFunc;
    public Action<UObject[]> sharpFunc;
}

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance;
    string m_BaseDownloadingURL = "";
    string[] m_AllManifest = null;
    AssetBundleManifest m_AssetBundleManifest = null;
    Dictionary<string, string[]> m_Dependencies = new Dictionary<string, string[]>();
    Dictionary<string, AssetBundleInfo> m_LoadedAssetBundles = new Dictionary<string, AssetBundleInfo>();
    Dictionary<string, List<LoadAssetRequest>> m_LoadRequests = new Dictionary<string, List<LoadAssetRequest>>();
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public Dictionary<string,AssetBundleInfo> GetLoadedBundles() {
        return m_LoadedAssetBundles;
    }

    public void Init(string mainifestName, Action initOK) {
        m_BaseDownloadingURL = Tools.GetRelativePath();
        LoadAsset<AssetBundleManifest>(mainifestName, new string[] {"AssetBundleManifest"}, delegate(UObject[] objs)
        {
            if (objs.Length > 0)
            {
                m_AssetBundleManifest = objs[0] as AssetBundleManifest;
                m_AllManifest = m_AssetBundleManifest.GetAllAssetBundles();
            }

            if (initOK != null) initOK();
        });
    }

    public void LoadAsset<T>(string abName, string[] assetsNames, Action<UObject[]> callback = null)
         where T : UnityEngine.Object
    {
        abName = GetRealAssetPath(abName);
        LoadAssetRequest request = new LoadAssetRequest();
        request.assetType = typeof(T);
        request.assetNames = assetsNames;
        request.sharpFunc = callback;

        List<LoadAssetRequest> requestList = null;
        if (!m_LoadRequests.TryGetValue(abName, out requestList))
        {
            requestList = new List<LoadAssetRequest>();
            requestList.Add(request);
            m_LoadRequests.Add(abName, requestList);
            StartCoroutine(OnLoadAsset<T>(abName));
        }
    }

    IEnumerator OnLoadAsset<T>(string name) where T : UObject
    {
        AssetBundleInfo bundleInfo = GetLoadedAssetBundle(name);
        if (bundleInfo == null)
        {
            yield return StartCoroutine(OnLoadAssetBundle(name, typeof(T)));

            bundleInfo = GetLoadedAssetBundle(name);
            if (bundleInfo == null)
            {
                m_LoadRequests.Remove(name);
                Debug.LogError("OnLoadAsset--->>>" + name);
                yield break;
            }
        }

        List<LoadAssetRequest> list = null;
        if (!m_LoadRequests.TryGetValue(name, out list))
        {
            m_LoadRequests.Remove(name);
            yield break;
        }
        
        for (int i = 0; i < list.Count; i++)
        {
            string[] assetNames = list[i].assetNames;
            List<UObject> result = new List<UObject>();

            AssetBundle ab = bundleInfo.m_AssetBundle;
            for (int j = 0; j < assetNames.Length; j++)
            {
                string assetPath = assetNames[j];
                AssetBundleRequest request = ab.LoadAssetAsync(assetPath, list[i].assetType);
                yield return request;
                result.Add(request.asset);
            }

            if (list[i].sharpFunc != null)
            {
                list[i].sharpFunc(result.ToArray());
                list[i].sharpFunc = null;
            }

            bundleInfo.m_ReferencedCount++;
        }

        m_LoadRequests.Remove(name);
    }

    IEnumerator OnLoadAssetBundle(string name, Type type) {
        string url = m_BaseDownloadingURL + name;
        WWW downLoad = null;
        if (type == typeof(AssetBundleManifest))
        {
            downLoad = new WWW(url);
        }
        else {
            string[] dependencies = m_AssetBundleManifest.GetAllDependencies(name);
            if (dependencies.Length > 0)
            {
                m_Dependencies.Add(name, dependencies);
                for (int i = 0; i < dependencies.Length; i++)
                {
                    string dependName = dependencies[i];
                    AssetBundleInfo bundleInfo = null;
                    
                    if (m_LoadedAssetBundles.TryGetValue(dependName, out bundleInfo))
                    {
                        bundleInfo.m_ReferencedCount++;
                    }
                    else if(!m_LoadRequests.ContainsKey(dependName)) {
                        yield return StartCoroutine(OnLoadAssetBundle(dependName, type));
                    }
                }
            }
            downLoad = WWW.LoadFromCacheOrDownload(url, m_AssetBundleManifest.GetAssetBundleHash(name), 0);
        }
        yield return downLoad;
        AssetBundle assetObj = downLoad.assetBundle;
        if (assetObj != null)
        {
            m_LoadedAssetBundles.Add(name, new AssetBundleInfo(assetObj));
        }
    }

    string GetRealAssetPath(string name)
    {
        if (name.Equals(AppConst.AssetDir))
        {
            return name;
        }

        name = name.ToLower();
        if (!name.EndsWith(AppConst.ExtName))
        {
            name += AppConst.ExtName;
        }

        if (name.Contains("/"))
        {
            return name;
        }

        for (int i = 0; i < m_AllManifest.Length; i++)
        {
            int index = m_AllManifest[i].LastIndexOf('/');
            string path = m_AllManifest[i].Remove(0, index + 1);
            if (path.Equals(name))
            {
                return m_AllManifest[i];
            }
        }

        Debug.LogError("Get Real path error:>> " + name);
        return null;
    }
    
    public AssetBundleInfo GetLoadedAssetBundle(string abName) {
        AssetBundleInfo bundleInfo = null;
        m_LoadedAssetBundles.TryGetValue(abName, out bundleInfo);
        if (bundleInfo == null)
        {
            return null;
        }
        string[] dependencies = null;
        if (!m_Dependencies.TryGetValue(abName, out dependencies))
        {
            return bundleInfo;
        }
        foreach (var dependency in dependencies)
        {
            AssetBundleInfo dependBundle;
            m_LoadedAssetBundles.TryGetValue(dependency, out dependBundle);
            if (dependBundle == null)
            {
                return null;
            }
        }
        return bundleInfo;
    }
}
