﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using UObject = UnityEngine.Object;

enum BundleLoadState
{
    Unload,
    Loading,
    Loaded,
}

class LoadRequest
{
    public string assetName;
    public BundleLoadState loadState = BundleLoadState.Unload;
    public LoadRequest() { }
}

public class ResourceManagerNew : MonoBehaviour
{
    public static ResourceManagerNew instance;
    AssetBundleManifest m_AssetBundleManifest;

    Dictionary<string, AssetBundleManifest> _manifestList = new Dictionary<string, AssetBundleManifest>();

    Dictionary<string,AssetBundle> m_LoadedAssetBundles = new Dictionary<string, AssetBundle>();

    Dictionary<string,LoadRequest> m_LoadRequests = new Dictionary<string, LoadRequest>();

    public int bundleReference = 0;

    string url = "";

    private void Awake() {
        instance = this;
    }

    public void Init(System.Action loadedComplete = null) {

        void LoadMainifestAsset(string abName, System.Type type, System.Action<UObject> callback = null)
        {
            IEnumerator GetAssets()
            {
                url = Path.Combine(Tools.GetRelativePath(), abName);
                UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(url, 0);
                Debug.Log("Loading mainifest " + abName + "................................");
                yield return request.Send();

                if (type == typeof(AssetBundleManifest))
                {
                    AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
                    StartCoroutine(GetAssetBundleManifest(bundle));
                    yield break;
                }
            }

            IEnumerator GetAssetBundleManifest(AssetBundle mainfestBundle)
            {
                AssetBundleManifest manifest = mainfestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                _manifestList.Add(abName, manifest);
                yield return manifest;
                callback?.Invoke(manifest);
            }

            StartCoroutine(GetAssets());
        }

        void LoadAsestByQueue(LinkedList<string> bundleQueue)
        {
            IEnumerator GetAssets()
            {
                LinkedListNode<string> pointerNode = bundleQueue.First;
                while(pointerNode != null)
                {
                    string abName = pointerNode.Value;
                    //TODO : 如果不从文件系统读取，那就删掉这行并设置cdn的url
                    url = Path.Combine(Tools.GetRelativePath(), abName);
                    UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(url, 0);
                    Debug.Log("Loading " + abName + "................................");
                    yield return request.SendWebRequest();

                    LoadRequest loadRequest = new LoadRequest();
                    loadRequest.assetName = abName;
                    if (request.isNetworkError || request.isHttpError)
                    {
                        Debug.LogError("Network connect fail !!");
                        break;
                    }
                    if (!m_LoadRequests.ContainsKey(abName))
                    {
                        loadRequest.loadState = BundleLoadState.Loading;
                        m_LoadRequests.Add(abName, loadRequest);
                        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
                        yield return bundle;

                        if (bundle != null)
                        {
                            Debug.Log("Loaded " + abName + " complete !!");
                            m_LoadedAssetBundles.Add(abName.Replace(AppConst.ExtName, ""), bundle);
                            m_LoadRequests[abName].loadState = BundleLoadState.Loaded;
                            bundleReference++;
                        }
                        else
                        {
                            Debug.LogError("Loaded " + abName + " Failed !!");
                            loadRequest.loadState = BundleLoadState.Unload;
                        }

                        pointerNode = pointerNode.Next;
                    }
                    else
                    {
                        Debug.Log(abName + " has loaed. Try to get next asset");
                        pointerNode = pointerNode.Next;
                        continue;
                    }
                }
                loadedComplete?.Invoke();
            }
            StartCoroutine(GetAssets());
        }

        void StartLoadAssetsFromList(UObject ABListFromMainifest)
        {
            AssetBundleManifest maintifest = ABListFromMainifest as AssetBundleManifest;
            m_AssetBundleManifest = maintifest;

            string[] bundles = m_AssetBundleManifest.GetAllAssetBundles();
            LinkedList<string> bundleLoadQueue = new LinkedList<string>();
            LinkedListNode<string> pointerNode = null;
            LinkedListNode<string> dependencePointerNode = null;

            void CheckDependencies(string[] dependencies)
            {
                if (dependencies.Length > 0)
                {
                    for (int j = 0; j < dependencies.Length; j++)
                    {
                        string dependencyBundleName = dependencies[j];
                        if (j == 0)
                        {
                            bundleLoadQueue.AddBefore(pointerNode, dependencyBundleName);
                            dependencePointerNode = pointerNode.Previous;
                        }
                        else
                        {
                            bundleLoadQueue.AddBefore(dependencePointerNode, dependencyBundleName);
                            dependencePointerNode = dependencePointerNode.Previous;
                        }
                    }
                }
            }

            for (int i = 0; i < bundles.Length; i++)
            {
                string bundleName = bundles[i];
                if (bundleLoadQueue.Find(bundleName) != null) continue;

                if (bundleLoadQueue.Count == 0)
                {
                    bundleLoadQueue.AddFirst(bundleName);
                    pointerNode = bundleLoadQueue.First;
                    CheckDependencies(m_AssetBundleManifest.GetAllDependencies(bundleName));
                }
                else
                {
                    bundleLoadQueue.AddAfter(pointerNode, bundleName);
                    pointerNode = pointerNode.Next;
                    CheckDependencies(m_AssetBundleManifest.GetAllDependencies(bundleName));
                }
            }

            LoadAsestByQueue(bundleLoadQueue);
        }

        LoadMainifestAsset(AppConst.AssetDir, typeof(AssetBundleManifest), StartLoadAssetsFromList);
    }

    public Dictionary<string, AssetBundle> GetLoadedAssetBundles()
    {
        return m_LoadedAssetBundles;
    }

    public AssetBundle GetBundleFromLoadedBundles(string bundleName)
    {
        AssetBundle asb;
        m_LoadedAssetBundles.TryGetValue(bundleName.ToLower(), out asb);
        return asb;
    }
}
