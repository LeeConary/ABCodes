﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UObject = UnityEngine.Object;

public class Packager
{
    public static string platform = string.Empty;
    static List<string> paths = new List<string>();
    static List<string> files = new List<string>();
    static List<AssetBundleBuild> maps = new List<AssetBundleBuild>();
    static string[] exts = { ".txt", ".xml", ".lua", ".assetbundle", ".json" };

    static string AppDataPath
    {
        get
        {
            return Application.dataPath + "/ABAssets";
        }
    }

    //能不能复制
    static bool CanCopy(string ext)
    {
        foreach (string e in exts)
        {
            if (ext.Equals(e)) return true;
        }
        return false;
    }

    [MenuItem("BuildAssets/Build iPhone Resource", false, 100)]
    public static void BuildiPhoneResource()
    {
        BuildTarget target;
#if UNITY_5
        target = BuildTarget.iOS;
#else
        target = BuildTarget.iOS;
#endif
        BuildAssetResource(target);
    }

    [MenuItem("BuildAssets/Build Android Resource", false, 101)]
    public static void BuildAndroidResource()
    {
        BuildAssetResource(BuildTarget.Android);
    }

    [MenuItem("BuildAssets/Build Windows Resource", false, 102)]
    public static void BuildWindowsResource()
    {
        BuildAssetResource(BuildTarget.StandaloneWindows);
    }

    /// <summary>
    /// 生成绑定素材
    /// </summary>
    public static void BuildAssetResource(BuildTarget target = BuildTarget.StandaloneWindows)
    {
        if (Directory.Exists(Tools.DataPath))
        {
            Directory.Delete(Tools.DataPath, true);
        }
        string streamPath = Application.streamingAssetsPath;
        if (Directory.Exists(streamPath))
        {
            Directory.Delete(streamPath, true);
        }
        Directory.CreateDirectory(streamPath);
        AssetDatabase.Refresh();

        maps.Clear();
        //HandleLuaBundle();
        HandleResourcesBundle();

        string resPath = "Assets/" + AppConst.AssetDir;
        BuildPipeline.BuildAssetBundles(resPath, maps.ToArray(), BuildAssetBundleOptions.None, target);
        BuildFileIndex();

        string streamDir = Application.dataPath + "/" + AppConst.LuaTempDir;
        if (Directory.Exists(streamDir)) Directory.Delete(streamDir, true);
        AssetDatabase.Refresh();
    }

    //处理资源位文件
    static void HandleResourcesBundle()
    {
        string resPath = AppDataPath + "/" + AppConst.AssetDir + "/";
        if (!Directory.Exists(resPath)) Directory.CreateDirectory(resPath);
        AddMaps(AppDataPath);
    }

    static void AddMaps(string rootPath)
    {
        string[] dirs = Directory.GetDirectories(rootPath);
        string[] files = Directory.GetFiles(rootPath);

        if (dirs.Length <= 0 && files.Length <= 0) return;
        foreach (string dir in dirs)
        {
            AddMaps(dir.Replace('\\', '/'));
            string path = dir.Substring(dir.IndexOf("Assets"));
            Debug.Log(path);
            string fileName = path.Split('\\')[1];
            AddBuildMap(fileName + AppConst.ExtName, "*.prefab", path);
            AddBuildMap(fileName + AppConst.ExtName, "*.jpg", path);
            AddBuildMap(fileName + AppConst.ExtName, "*.png", path);
        }
    }

    //创建文件列表txt
    static void BuildFileIndex()
    {
        string resPath = Application.streamingAssetsPath;
        string newFilePath = resPath + "/files.txt";
        if (File.Exists(newFilePath))
        {
            File.Delete(newFilePath);
        }
        paths.Clear();
        files.Clear();
        //遍历目录及其子目录
        Recursive(resPath);

        FileStream fs = new FileStream(newFilePath, FileMode.CreateNew);
        StreamWriter sw = new StreamWriter(fs);
        for (int i = 0; i < files.Count; i++)
        {
            string file = files[i];
            string ext = Path.GetExtension(file);
            if (file.EndsWith(".meta") || file.Contains(".DS_Store")) continue;

            //TODO: 加入md5
            string md5 = file;
            string value = file.Replace(resPath, string.Empty);
            sw.WriteLine(value + "|" + md5);
        }
        sw.Close();
        fs.Close();
    }

    //添加资源包对象到列表中
    static void AddBuildMap(string fileName, string fileExt, string path)
    {
        string[] files = Directory.GetFiles(path, fileExt);
        if (files.Length == 0)
        {
            return;
        }

        for (int i = 0; i < files.Length; i++)
        {
            files[i] = files[i].Replace('\\', '/');
        }
        AssetBundleBuild assetBundleBuild = new AssetBundleBuild();
        assetBundleBuild.assetBundleName = fileName;
        assetBundleBuild.assetNames = files;
        maps.Add(assetBundleBuild);
    }

    //遍历目录及其子目录
    static void Recursive(string path)
    {
        string[] names = Directory.GetFiles(path);
        string[] dirs = Directory.GetDirectories(path);
        foreach (string fileName in names)
        {
            string ext = Path.GetExtension(fileName);
            if (ext.Equals(".meta"))
            {
                continue;
            }
            files.Add(fileName.Replace('\\', '/'));
        }
        foreach (string dir in dirs)
        {
            paths.Add(dir.Replace('\\', '/'));
            Recursive(dir);
        }
    }
}
